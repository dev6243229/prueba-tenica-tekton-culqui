### Count the all the letters in a string

```js
  public static void main(String[] args) {
    String myStr = "abcasdeacbs";
    Map<Character, Integer> result = countLettersInString(myStr);
    System.out.println("result => " + result);
  }

    public static Map<Character, Integer> countLettersInString(String myStr) {

    Map<Character, Integer> myMap = new HashMap<>();
    char[] charArray = myStr.toCharArray();

    for (char character : charArray) {
      if (myMap.get(character) != null && myMap.get(character) > 0) {
        myMap.put(character, myMap.get(character) + 1);
      } else {
        myMap.put(character, 1);
      }
    }
    return myMap;
  }
```
### Reverse the order of a list
```js
  public static void main(String[] args) {
    List<String> myList = Arrays.asList("Cat", "Dog", "Fish");
    System.out.println(myList);

    // reverse the order of the list
    Collections.reverse(myList);
    System.out.println(myList);
  }
```
### FizzBuzz
```js
  // Write a short program that prints each number from 1 to 100 on an new line
  //For each multiple of 3, print "Fizz" instead of the number
  // For each multiple of 5, print "Buzz" instead of the number
  // For Numbers which are multiples of both 3 and 5, print "FizzBuzz" instead of the number
  public static void main(String[] args) {

    printFizzBuzz(100);

  }
  public static void printFizzBuzz(int n) {
    for (int i = 1; i <= n; i++){
      if((i % 3 == 0) && (i % 5 == 0)) {
        System.out.println("FizzBuzz");
      } else if (i % 3 == 0) {
        System.out.println("Fizz");
      } else if (i % 5 == 0) {
        System.out.println("Buzz");
      } else {
        System.out.println(i);
      }
    }
  }
  ```

## Get the indexes of two values in the array to sum an expected value
```js
  // Given an array of integers, return indices of the two numbers such that they add up to a specific target
  // you may assume that each input would have return exactly one solution, and you may not use the same element twice
  public static void main(String[] args) {
    int[] numbers = new int[]{2, 3, 7, 4, 8};
    int target = 6;
    System.out.println(Arrays.toString(getTwoSum(numbers, target)));
  }

  public static int[] getTwoSum(int[] numbers, int target) {
    Map<Integer, Integer> myMap = new HashMap<>();
    for (int i = 0; i < numbers.length; i++) {
      int delta = target - numbers[i];
      if (myMap.containsKey(delta)) {
        return new int[]{i, myMap.get(delta)};
      }
      myMap.put(numbers[i], i);
    }

    return new int[]{-1, -1};
  }
```


## Reverse the value of a string
```js
  public static void main(String[] args) {
    String str = "Hello World!";
    System.out.println(reverseStringWithStringBuilder(str));
    System.out.println(reverseStringManually(str));
    System.out.println(reverseStringManuallyWithStringBuilder(str));

  }

  public static String reverseStringWithStringBuilder(String str) {
    return new StringBuilder((str)).reverse().toString();
  }

  public static String reverseStringManually(String str) {
    StringBuilder sb = new StringBuilder();
    for(int i = str.length() -1;  i >= 0; i--){
      sb.append(str.charAt((i)));
    }
    return sb.toString();
  }

  public static String reverseStringManuallyWithStringBuilder(String str) {
    String result = "";
    for(int i = str.length() -1;  i >= 0; i--){
      result = result.concat(String.valueOf(str.charAt((i))));
    }
    return result;
  }
```


## Create a Stack in Java
```js
public class Stack {

  private int array[];
  private int top;
  private int capacity;

  public Stack(int capacity) {
    this.array  = new int[capacity];
    this.capacity = capacity;
    this.top = -1;
  }

  public void push(int item) throws Exception {
    if(top == capacity - 1){
      throw new Exception("Stack is full");
    }
    array[++top] = item;
  }


  public int pop() throws Exception {
    if(top == - 1){
      throw new Exception("Stack is Empty");
    }
    return array[top--];
  }

  public int peek() throws Exception {
    if(top == - 1){
      throw new Exception("Stack is Empty");
    }
    return array[top];
  }
}
```

## Convert Integer to Roman Number
```js
  public static void main(String[] args) {
    System.out.println(convertIntToRoman(22));
  }

  public static String convertIntToRoman(int input) {
    String[] units = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
    String[] tens = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
    String[] hundreds = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
    String[] thousands = {"", "M", "MM", "MMM"};
    return thousands[input / 1000] + hundreds[(input % 1000) / 100] + tens[(input % 100) / 10]
        + units[input % 10];
  }
```

## Find First repeating Character
```js
public static void main(String[] args) {
    System.out.println(findFirstRepeatingCharacter("responsible"));
  }

  public static String findFirstRepeatingCharacter(String str) {
     String result= null;
    Map<Character, Integer> myMap = new HashMap<>();
    char[] charArray = str.toCharArray();

    for (char character : charArray) {
      if (myMap.get(character) != null && myMap.get(character) > 0) {
        myMap.put(character, myMap.get(character) + 1);
        result = String.valueOf(character);
        break;
      } else {
        myMap.put(character, 1);
      }
    }
    return result;
  }
  ```

## Remove Duplicates on array
```js
    //Given an array of integers arr, create a function that returns an array
  // containing the values of arr without duplicates (the order doesn't matter).
  public static void main(String[] args) {
    int[] arr = new int[]{4, 2, 5, 3, 3, 1, 2, 4, 1, 5, 5, 5, 3, 1};
    // Output: [4, 2, 5, 3, 1]
    System.out.println(Arrays.toString(removeDuplicates(arr)));
  }

  public static int[] removeDuplicates(int[] arr) {
    int[] noDuplicates;
    Set<Integer> setNumbers = new HashSet<>();
    for (int value : arr) {
      setNumbers.add(value);
    }
    noDuplicates = setNumbers.stream().mapToInt(Integer::intValue).toArray();
    return noDuplicates;
  }
```
